# PIC16F1459-USB

This is a board I designed for my own use.

![alt text](Render/BreadboardPIC.png "Preview of the PIC16F1459-USB Breadboard PIC Board")

The goal was:
* be able to use a PIC16F1459 on a breadboard, which meant keeping the board small and minimal;
* simultaneously have the possibility to easily connected a PICKIT for programming;
* also have the possibility to connect to the USB interface for external communication if desired

I decided for a 4-layer board (probably overkill for the intended use), but
it was my first time experimenting with a 4-layer board and impedance controlled
traces for the USB, even though the length is so small it is probably not necessary.

The current board can probably be improved on a couple of fronts:
* USB traces should probably not be routed to the breadboard;
* The VUSB from the USB plug could be connected to a pin of the PIC for monitoring;
* External pull-up for the D+ line (instead of internal), which would allow the PIC to identify if it is connected to a USB host;
* Consider the possibility of other powering options, such as power from USB;
